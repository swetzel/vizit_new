Physical rotation trajectories from the [*Rotate it!*](https://www.researchgate.net/publication/309294549_Rotate_it_-_Effects_of_touch-based_gestures_on_elementary_school_students%27_solving_of_mental_rotation_tasks) study.
![rotate_it.PNG](https://bitbucket.org/repo/6ELj8A/images/671861611-rotate_it.PNG)


Binned Rotation Trajectories 
![rotate_it_bins.PNG](https://bitbucket.org/repo/6ELj8A/images/1097306151-rotate_it_bins.PNG)
 
Clustered rotation trajectories
![rotate_it_cluster.PNG](https://bitbucket.org/repo/6ELj8A/images/2581996174-rotate_it_cluster.PNG)

Contact: stefanie.wetzel@gmail.com