package data;

import java.util.ArrayList;

import com.sun.javafx.collections.SetAdapterChange;

public class Subject {

	private String id;
	private float successRate;
	private float duration;
	private int correctAnswers;
	private int givenAnswers;
	private ArrayList<Integer> answers;
	private String group;
	
	

	public Subject(String id)
	{
		setId(id);
	}
	
	public Subject (String id, int correctAnswers, int answersGiven, float duration, ArrayList<Integer> answers, String group){
		
		setId(id);
		setCorrectAnswers(correctAnswers);
		setGivenAnswers(answersGiven);
		setDuration(duration);
		setAnswers(answers);
		setGroup(group);
		
		setSuccessRate((float)correctAnswers/(float)answersGiven);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getSuccessRate() {
		return successRate;
	}
	public void setSuccessRate(float successRate) {
		this.successRate = successRate;
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public int getCorrectAnswers() {
		return correctAnswers;
	}

	public void setCorrectAnswers(int correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

	public int getGivenAnswers() {
		return givenAnswers;
	}

	public void setGivenAnswers(int givenAnswers) {
		this.givenAnswers = givenAnswers;
	}

	public ArrayList<Integer> getAnswers() {
		return answers;
	}

	public void setAnswers(ArrayList<Integer> answers) {
		this.answers = answers;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
}
