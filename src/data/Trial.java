package data;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;






import view.GUI;
import view.Menu;


public class Trial {

	private boolean correctAnswer;
	private ArrayList<FeatureVector> data = new ArrayList<FeatureVector>();
	private ArrayList<Drag> drags = new ArrayList<Drag>();
	private String groupID;
	private int id;
	private String subject;
	private String taskID;
	private boolean sameTrial;
	private FeatureVector minFeatures;
	private FeatureVector maxFeatures;
	private boolean isFiltered = false;
	private float index = 0;	// helper for normalization
	private float physicalPhaseStart;
	private float physicalPhaseEnd;
	private float successRate;
	private int condition; // 1 = mental, 0 = manual
	private int response; // 0 = left, 1 = right
	private int trialBlock;
	private int taskNumber;	// Formally known as taskID 
	private int taskPresented; // when was the task presented within the trialblock (1-90)
	
	
	public Trial(String subject, String taskID, int id, boolean sameTrial, int condition, int response, int trialblock, int taskNumber, int taskPresented){
		
		setTaskID(taskID);
		setSubject(subject);
		setId(id);
		setSameTrial(sameTrial);
		setCorrectAnswer(correctAnswer);
		setCondition(condition);
		setResponse(response);
		setTrialBlock(trialblock);
		setTaskNumber(taskNumber);
		setTaskPresented(taskPresented);
	}
	
	public Trial(String subject, String taskID, int id, boolean sameTrial, int condition, int response, int trialblock, int taskNumber, int taskPresented, boolean answer){
	
		setTaskID(taskID);
		setSubject(subject);
		setId(id);
		setSameTrial(sameTrial);
		setCorrectAnswer(answer);
		setCondition(condition);
		setResponse(response);
		setTrialBlock(trialblock);
		setTaskNumber(taskNumber);
		setTaskPresented(taskPresented);
		
	}
	
	
	public void setDrags(ArrayList<Drag> drags) {
		this.drags = drags;
	}
	
	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	
	public ArrayList<Drag> getDrags() {
		return drags;
	}
	
	public float getInitialAD(){
		
		return data.get(0).getAngularDisparity();
	}
	
	
	public float getFinalAD(){
		return data.get(data.size()-1).getAngularDisparity();
	}
	
	public float getAccumulatedWay(){
		
		return data.get(data.size()-1).getAccumulatedWay();
	}
	
	public float getAccumulatedWayInPhase(float phase){
		
		return data.get(data.size()-1).getAccumulatedWay();
	}
	
	public float getDuration(){
		
		return data.get(data.size()-1).getTimestamp();
	}
	
	public float getPhyisicalPhaseLenght(){
		
		return physicalPhaseEnd-physicalPhaseStart;
	}
	
	
	public float getAverageVelocity(){
		
		float velo = 0;
		
		for(FeatureVector fv: data){
			
			velo += fv.getVelocity(); 
		}
		
		return (float) velo/data.size();
	}
	
	
	
	public void calcAccumWay(){
		
		float sum = 0;
		
		if (data != null){
			for (FeatureVector fv : data){
				sum += fv.getVelocity();
				fv.setAccumulatedWay(sum);
			}
		}
	}
	
	public void setMinMaxFeatures(FeatureVector min, FeatureVector max){
		
		minFeatures.setAngularDisparity(min.getAngularDisparity());
		maxFeatures.setAngularDisparity(max.getAngularDisparity());
		minFeatures.setVelocity(min.getVelocity());
		maxFeatures.setVelocity(max.getVelocity());
		minFeatures.setEulerX(min.getEulerX());
		maxFeatures.setEulerX(max.getEulerX());
		minFeatures.setEulerY(min.getEulerY());
		maxFeatures.setEulerY(max.getEulerY());
		minFeatures.setEulerZ(min.getEulerZ());
		maxFeatures.setEulerZ(max.getEulerZ());
		minFeatures.setViewportX(min.getViewportX());
		maxFeatures.setViewportX(max.getViewportX());
		minFeatures.setViewportY(min.getViewportY());
		maxFeatures.setViewportY(max.getViewportY());
		minFeatures.setAccumulatedWay(min.getAccumulatedWay());
		maxFeatures.setAccumulatedWay(max.getAccumulatedWay());
	}
	
	
	public void findMinMaxFeatures(){
		
		float minTime = 10000;
		float maxTime = -10000;
		float minD1 = 10000;
		float maxD1 = -10000;
		float minD2 = 10000;
		float maxD2 = -10000;
		float minD3 = 10000;
		float maxD3 = -10000;
		float minD4 = 10000;
		float maxD4 = -10000;
		float minD5 = 10000;
		float maxD5 = -10000;
		float minD6 = 10000;
		float maxD6 = -10000;
		float minD7 = 10000;
		float maxD7 = -10000;
		float minD8 = 10000;
		float maxD8 = -10000;
		float minD9 = 10000;
		float maxD9 = -10000;
		float minD10 = 10000;
		float maxD10 = -10000;
		float minD11 = 10000;
		float maxD11 = -10000;
		float minD12 = 10000;
		float maxD12 = -10000;
		float minD13 = 10000;
		float maxD13 = -10000;
		
		for (FeatureVector fv: data){
			
			minTime = Math.min(minTime, fv.getTimestamp());
			maxTime = Math.max(maxTime, fv.getTimestamp());
			minD1 = Math.min(minD1, fv.getVelocity());
			maxD1 = Math.max(maxD1, fv.getVelocity());
			minD2 = Math.min(minD2, fv.getAngularDisparity());
			maxD2 = Math.max(maxD2, fv.getAngularDisparity());
			minD3 =  Math.min(minD3, fv.getEulerX());
			maxD3 =  Math.max(maxD3, fv.getEulerX());
			minD4 =  Math.min(minD4, fv.getEulerY());
			maxD4 =  Math.max(maxD4, fv.getEulerY());
			minD5 =  Math.min(minD5, fv.getEulerZ());
			maxD5 =  Math.max(maxD5, fv.getEulerZ());
			minD6 = Math.min(minD6, fv.getVisibleArea());
			maxD6 = Math.max(maxD6, fv.getVisibleArea());
			minD7 = Math.min(minD7, fv.getVisibleAreaAccumulated());
			maxD7 = Math.max(maxD7, fv.getVisibleAreaAccumulated());
			minD8 = Math.min(minD8, fv.getViewportX());
			maxD8 = Math.max(maxD8, fv.getViewportX());
			minD9 = Math.min(minD9, fv.getViewportY());
			maxD9 = Math.max(maxD9, fv.getViewportY());
			minD10 = Math.min(minD10, fv.getAccumulatedWay());
			maxD10 = Math.max(maxD10, fv.getAccumulatedWay());
			minD11 = Math.min(minD11, fv.getEulerXDelta());
			maxD11 = Math.max(maxD11, fv.getEulerXDelta());
			minD12 = Math.min(minD12, fv.getEulerYDelta());
			maxD12 = Math.max(maxD12, fv.getEulerYDelta());
			minD13 = Math.min(minD13, fv.getEulerZDelta());
			maxD13 = Math.max(maxD13, fv.getEulerZDelta());
		}
		
		minFeatures = new FeatureVector(minTime, minD1, minD2, minD3, minD4, minD5, minD11, minD12, minD13, minD6, minD7, minD8, minD9, minD10);
		maxFeatures = new FeatureVector(maxTime, maxD1, maxD2, maxD3, maxD4, maxD5, maxD11, maxD12, maxD13, maxD6, maxD7, maxD8, maxD9, maxD10);
	}
	
	public ArrayList<FeatureVector> getData() {
		return data;
	}
	public void setData(ArrayList<FeatureVector> data) {
		this.data = data;
	}
	
	public void addData(FeatureVector fv){
		data.add(fv);
	}
	
	public boolean isCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(boolean correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	
	
	
	
	
	public int drawFeature(Color c, Graphics2D g2d,float t, float feature,float minFeature,float maxFeature, int minX, int minY, int lengthX, int lengthY,int lastX,int lastY, int clusterID){
		
		int x = 0;
		int y = 0;
		

		float xD2 = 0;
		if (Menu.getInstance().getCheckBoxNormalize().isSelected()){
			
			xD2 = (float)t/maxFeatures.getTimestamp();
		}
	
		else{
			xD2 = (float)t/GUI.getInstance().getMaxFeatures().getTimestamp();
		}
		
		
		float yD2 = (feature - minFeature)/(Math.abs(minFeature - maxFeature));
	
		
	
		x = (int)(minX + lengthX*xD2);
		y = (int) (minY - lengthY*yD2);
		
		/*
		if (clusterID != -1){
			g2d.setColor(new Color(200*clusterID,20, 20));
			g2d.fillOval(x,y, 5, 5);
		}
		*/
		g2d.setColor(c);
		if (lastY == 0)
			lastY = y;
		
		g2d.drawLine(x, y, lastX, lastY);
		
		return y;
	}
	
	public void findStartOfPhysicalPhase(){
		
		for(FeatureVector fv: data){
			
			if (fv.getVelocity() > 0){
				setPhysicalPhaseStart(fv.getTimestamp());
				break;
			}
		}
	}
	
	public void findEndOfPhysicalPhase(){
		
		for(int j = data.size() - 1; j >= 0; j--){
					
			if (data.get(j).getVelocity() > 0){
				setPhysicalPhaseEnd(data.get(j).getTimestamp());
				break;
			}
		}
	}
	
	public void draw(Graphics2D g2d,Color c, int minX, int minY, int lengthX, int lengthY){
				
		int lastX = 50;
		int lastD1 = 0;
		int lastD2 = 0;
		int lastD3 = 0;
		int lastD4 = 0;
		int lastD5 = 0;
		int lastD6 = 0;
		int lastD7 = 0;
		int lastD8 = 0;		
		int lastD9 = 0;		
		int lastD10 = 0;		
		int lastD11 = 0;		
		
		for (FeatureVector fv: data){
			
			if (Menu.getInstance().getCheckBoxAD().isSelected()){
				
				lastD1 = drawFeature(c,g2d,fv.getTimestamp(),fv.getAngularDisparity(),minFeatures.getAngularDisparity(),maxFeatures.getAngularDisparity(),minX, minY, lengthX, lengthY,lastX, lastD1, fv.getClusterID());
				
			}
			if (Menu.getInstance().getCheckBoxVelo().isSelected()){
				
					lastD2 = drawFeature(c,g2d,fv.getTimestamp(),fv.getVelocity(),minFeatures.getVelocity(),maxFeatures.getVelocity(), minX, minY, lengthX, lengthY,lastX, lastD2, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxAxisX().isSelected()){
					
					lastD3 = drawFeature(c,g2d,fv.getTimestamp(),fv.getEulerX(),minFeatures.getEulerX(),maxFeatures.getEulerX(), minX, minY, lengthX, lengthY,lastX, lastD3, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxAxisY().isSelected()){
	
					lastD4 = drawFeature(c,g2d,fv.getTimestamp(),fv.getEulerY(),minFeatures.getEulerY(),maxFeatures.getEulerY(), minX, minY, lengthX, lengthY,lastX, lastD4, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxAxisZ().isSelected()){
	
					lastD5 = drawFeature(c,g2d,fv.getTimestamp(),fv.getEulerZ(),minFeatures.getEulerZ(),maxFeatures.getEulerZ(), minX, minY, lengthX, lengthY,lastX, lastD5, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxViewportX().isSelected()){
				
					lastD6 = drawFeature(c,g2d,fv.getTimestamp(),fv.getViewportX(),minFeatures.getViewportX(),maxFeatures.getViewportX(), minX, minY, lengthX, lengthY,lastX, lastD6, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxViewportY().isSelected()){
				
					lastD7 = drawFeature(c,g2d,fv.getTimestamp(),fv.getViewportY(),minFeatures.getViewportY(),maxFeatures.getViewportY(), minX, minY, lengthX, lengthY,lastX, lastD7, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxAccWay().isSelected()){
			
					lastD8 = drawFeature(c,g2d,fv.getTimestamp(),fv.getAccumulatedWay(),minFeatures.getAccumulatedWay(),maxFeatures.getAccumulatedWay(), minX, minY, lengthX, lengthY,lastX, lastD8, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxDeltaX().isSelected()){
							
					lastD9 = drawFeature(c,g2d,fv.getTimestamp(),fv.getEulerXDelta(),minFeatures.getEulerXDelta(),maxFeatures.getEulerXDelta(), minX, minY, lengthX, lengthY,lastX, lastD9, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxDeltaY().isSelected()){
				
					lastD10 = drawFeature(c,g2d,fv.getTimestamp(),fv.getEulerYDelta(),minFeatures.getEulerYDelta(),maxFeatures.getEulerYDelta(), minX, minY, lengthX, lengthY,lastX, lastD10, fv.getClusterID());
			}
			if (Menu.getInstance().getCheckBoxDeltaZ().isSelected()){
				
					lastD11 = drawFeature(c,g2d,fv.getTimestamp(),fv.getEulerZDelta(),minFeatures.getEulerZDelta(),maxFeatures.getEulerZDelta(), minX, minY, lengthX, lengthY,lastX, lastD11, fv.getClusterID());
			}
			
			
			
			if (Menu.getInstance().getCheckNormalize().isSelected()){
				
							
				lastX = (int) (minX + ((float)fv.getTimestamp()/maxFeatures.getTimestamp())*lengthX);
				
			}
			else {
				lastX = (int) (minX + ((float)fv.getTimestamp()/GUI.getInstance().getMaxFeatures().getTimestamp())*lengthX);
			}
			
			
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isSameTrial() {
		return sameTrial;
	}

	public void setSameTrial(boolean sameTrial) {
		this.sameTrial = sameTrial;
	}

	public boolean isFiltered() {
		return isFiltered;
	}

	public void setFiltered(boolean isFiltered) {
		this.isFiltered = isFiltered;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTaskID() {
		return taskID;
	}

	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public void clearCluster() {
		
		for(FeatureVector fv : data){
			fv.setClusterID(-1);
		}
	}

	public FeatureVector getMinFeatures() {
		return minFeatures;
	}

	public FeatureVector getMaxFeatures() {
		return maxFeatures;
	}

	public float getIndex() {
		return index;
	}

	public void setIndex(float index) {
		this.index = index;
	}

	public float getPhysicalPhaseStart() {
		return physicalPhaseStart;
	}
	
	public float getPhysicalPhaseStartRelative() {
		return physicalPhaseStart/data.get(data.size()-1).getTimestamp();
	}

	public void setPhysicalPhaseStart(float physicalPhaseStart) {
		this.physicalPhaseStart = physicalPhaseStart;
	}

	public void calcDrags() {
		
		float lastVelo = 0;
		boolean isDrag = false;
		Drag drag = null;
		
		for (FeatureVector fv: data){
			
			if (fv.getVelocity() > 0 && !isDrag){
				
				isDrag = true;
				drag = new Drag(fv.getTimestamp());
			}
			if (lastVelo > 0 && fv.getVelocity() == 0 && isDrag){
				
				isDrag = false;
				drag.setEnd(fv.getTimestamp());
				drags.add(drag);
				drag = null;
			}
				
			lastVelo = fv.getVelocity();
		}
	}

	public float getPhysicalPhaseEnd() {
		return physicalPhaseEnd;
	}

	public float getPhysicalPhaseEndRelative() {
		return physicalPhaseEnd/data.get(data.size()-1).getTimestamp();
	}
	
	public void setPhysicalPhaseEnd(float physicalPhaseEnd) {
		this.physicalPhaseEnd = physicalPhaseEnd;
	}

	public float getSuccessRate() {
		return successRate;
	}

	public void setSuccessRate(float successRate) {
		this.successRate = successRate;
	}

	public int getCondition() {
		return condition;
	}

	public void setCondition(int condition) {
		this.condition = condition;
	}

	public int getTrialBlock() {
		return trialBlock;
	}

	public void setTrialBlock(int trialBlock) {
		this.trialBlock = trialBlock;
	}

	public int getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}

	public int getTaskPresented() {
		return taskPresented;
	}

	public void setTaskPresented(int taskPresented) {
		this.taskPresented = taskPresented;
	}

	public int isResponse() {
		return response;
	}

	public void setResponse(int response) {
		this.response = response;
	}
}
