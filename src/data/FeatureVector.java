package data;

public class FeatureVector {

	private float timestamp;

	private float velocity;
	private float angularDisparity;
	private float eulerX;
	private float eulerY;
	private float eulerZ;
	private float eulerXDelta;
	private float eulerYDelta;
	private float eulerZDelta;
	private float visibleArea;
	private float visibleAreaAccumulated;
	private float viewportX;
	private float viewportY;
	private float accumulatedWay;
	
	
	private int clusterID;
	
	
	
	
	public FeatureVector(){}
	
	public FeatureVector(float time, float velo, float ad, float eX, float eY, float eZ, float eXDelta, float eYDelta, float eZDelta, float visArea, float visAreaAcc, float vX, float vY){
		
		setTimestamp(time);
		setVelocity(velo);
		setAngularDisparity(ad);
		setEulerX(eX);
		setEulerY(eY);
		setEulerZ(eZ);
		setEulerXDelta(eXDelta);
		setEulerYDelta(eYDelta);
		setEulerZDelta(eZDelta);
		setVisibleArea(visArea);
		setVisibleAreaAccumulated(visAreaAcc);
		setViewportX(vX);
		setViewportY(vY);
		
	}
	
	public FeatureVector(float time, float velo, float ad, float eX, float eY, float eZ,float eXDelta, float eYDelta, float eZDelta, float visArea, float visAreaAcc, float vX, float vY, float accumulatedWay){
		
		setTimestamp(time);
		setVelocity(velo);
		setAngularDisparity(ad);
		setEulerX(eX);
		setEulerY(eY);
		setEulerZ(eZ);
		setEulerXDelta(eXDelta);
		setEulerYDelta(eYDelta);
		setEulerZDelta(eZDelta);
		setVisibleArea(visArea);
		setVisibleAreaAccumulated(visAreaAcc);
		setViewportX(vX);
		setViewportY(vY);
		setAccumulatedWay(accumulatedWay);
		
	}

	
	
	
	

	public float getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(float timestamp) {
		this.timestamp = timestamp;
	}


	


	public int getClusterID() {
		return clusterID;
	}


	public void setClusterID(int clusterID) {
		this.clusterID = clusterID;
	}


	
	public float getVelocity() {
		return velocity;
	}





	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}





	public float getAngularDisparity() {
		return angularDisparity;
	}





	public void setAngularDisparity(float angularDisparity) {
		this.angularDisparity = angularDisparity;
	}





	public float getEulerX() {
		return eulerX;
	}





	public void setEulerX(float eulerX) {
		this.eulerX = eulerX;
	}





	public float getEulerY() {
		return eulerY;
	}





	public void setEulerY(float eulerY) {
		this.eulerY = eulerY;
	}





	public float getEulerZ() {
		return eulerZ;
	}





	public void setEulerZ(float eulerZ) {
		this.eulerZ = eulerZ;
	}





	public float getVisibleArea() {
		return visibleArea;
	}





	public void setVisibleArea(float visibleArea) {
		this.visibleArea = visibleArea;
	}





	public float getVisibleAreaAccumulated() {
		return visibleAreaAccumulated;
	}





	public void setVisibleAreaAccumulated(float visibleAreaAccumulated) {
		this.visibleAreaAccumulated = visibleAreaAccumulated;
	}





	public float getViewportX() {
		return viewportX;
	}





	public void setViewportX(float viewportX) {
		this.viewportX = viewportX;
	}





	public float getViewportY() {
		return viewportY;
	}





	public void setViewportY(float viewportY) {
		this.viewportY = viewportY;
	}

	

	public float getAccumulatedWay() {
		return accumulatedWay;
	}

	public void setAccumulatedWay(float accumulatedWay) {
		this.accumulatedWay = accumulatedWay;
	}

	public float getEulerXDelta() {
		return eulerXDelta;
	}

	public void setEulerXDelta(float eulerXDelta) {
		this.eulerXDelta = eulerXDelta;
	}

	public float getEulerYDelta() {
		return eulerYDelta;
	}

	public void setEulerYDelta(float eulerYDelta) {
		this.eulerYDelta = eulerYDelta;
	}

	public float getEulerZDelta() {
		return eulerZDelta;
	}

	public void setEulerZDelta(float eulerZDelta) {
		this.eulerZDelta = eulerZDelta;
	}

	
	
	
}
