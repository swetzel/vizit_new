package view;

import handler.PopUpListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class PopUpMenu extends JPopupMenu {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JMenuItem trial;
	
    public PopUpMenu(int subjectIndex, int experimentIndex){
    	
    	PopUpListener listener = new PopUpListener();
    	
    	trial = new JMenuItem("Display Trial");
        add(trial);
        trial.addActionListener(listener);
        trial.setActionCommand("trial "+ subjectIndex+" "+experimentIndex);
        
        
    }
}
