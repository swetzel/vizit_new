package view;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JPanel;

import data.FeatureVector;
import data.Trial;


public class ImagePanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Trial> correctTrials = null;
	private ArrayList<Trial> incorrectTrials = null;
	private float startPhaseCorrect = 0;
	private float endPhaseCorrect = 0;
	private float startPhaseIncorrect =0;
	private float endPhaseIncorrect = 0;

	private int minX = 50;
	private int minY = 20;
	private int maxX = this.getWidth()-10;
	private int maxY = this.getHeight()-30;
	
	public ImagePanel(){
		setBackground(Color.WHITE);
		
	}

	
	public void setTrials(ArrayList<Trial> correctTrials, ArrayList<Trial> incorrectTrials){
		
		this.correctTrials = correctTrials;
		this.incorrectTrials = incorrectTrials;
		repaint();
		
	}
	
	
	
	public void resize(int width, int height){
		
		maxX = width-10;
		maxY = height-30;
		this.repaint();
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
				
    	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
    	g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
    			RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	     
    	if (Menu.getInstance().getCheckBoxNormalize().isSelected()){
	    	
    		g2d.drawLine(minX, minY, minX, maxY);
	    	g2d.drawLine(minX, maxY, maxX, maxY);
	    	
	    	g2d.setFont(new Font("Arial", Font.PLAIN, 20));   	
	    
	    	
	    	// x-axis
	    	g2d.drawString("0", minX, maxY + 25);
	    	g2d.drawString("0.25", ((maxX-minX)/4) +5 ,maxY + 25);
	    	g2d.drawLine(((maxX-minX)/4) + minX, maxY + 5, ((maxX-minX)/4) + minX, maxY - 5);
	    	g2d.drawString("0.5", ((maxX-minX)/2) +10 ,maxY + 25);
	    	g2d.drawLine(((maxX-minX)/2) + minX, maxY + 5, ((maxX-minX)/2) + minX, maxY - 5);
	    	g2d.drawString("0.75", (((maxX-minX)/4)*3) +5 ,maxY + 25);
	    	g2d.drawLine(((maxX-minX)/4)*3 + minX, maxY + 5, ((maxX-minX)/4)*3 + minX, maxY - 5);
	    	g2d.drawString("1", maxX-5, maxY + 25);
	    	g2d.drawLine(maxX, maxY + 5, maxX, maxY - 5);
	    	
	    	// y-axis
	    	g2d.drawString("0�", minX-40, maxY );
	    	g2d.drawString("30�", minX-45, ((maxY-minY)/6)*5 +5  );
	    	g2d.drawLine(minX-5, ((maxY-minY)/6)*5  , minX+5, ((maxY-minY)/6)*5 );
	    	g2d.drawString("60�", minX-45, ((maxY-minY)/6)*4 +5  );
	    	g2d.drawLine(minX-5, ((maxY-minY)/6)*4  , minX+5, ((maxY-minY)/6)*4 );
	    	g2d.drawString("90�", minX-45, ((maxY-minY)/6)*3 +5  );
	    	g2d.drawLine(minX-5, ((maxY-minY)/6)*3  , minX+5, ((maxY-minY)/6)*3 );
	    	g2d.drawString("120�", minX-45, ((maxY-minY)/6)*2 +5  );
	    	g2d.drawLine(minX-5, ((maxY-minY)/6)*2  , minX+5, ((maxY-minY)/6)*2 );
	    	g2d.drawString("150�", minX-45, ((maxY-minY)/6) +5  );
	    	g2d.drawLine(minX-5, ((maxY-minY)/6)  , minX+5, ((maxY-minY)/6) );
	    	g2d.drawString("180�", minX-45, minY +10 );
	    	g2d.drawLine(minX-5, maxY , minX+5, maxY);
	    	
	    	//start and end phase
	    	/*
	    	g2d.setColor(new Color(0,0,205));
	    	g2d.drawLine(((int)((maxX-minX)*startPhaseCorrect) + minX), maxY + 5,((int)((maxX-minX)*startPhaseCorrect) + minX), minY);
	    	g2d.setColor(Color.red);
	    	g2d.drawLine(((int)((maxX-minX)*startPhaseIncorrect) + minX), maxY + 5,((int)((maxX-minX)*startPhaseIncorrect) + minX), minY);
	    	
	    	g2d.setColor(new Color(0,0,205));
	    	g2d.drawLine(((int)((maxX-minX)*endPhaseCorrect) + minX), maxY + 5, ((int)((maxX-minX)*endPhaseCorrect) + minX), minY);
	    	g2d.setColor(Color.red);
	    	g2d.drawLine(((int)((maxX-minX)*endPhaseIncorrect) + minX), maxY + 5, ((int)((maxX-minX)*endPhaseIncorrect) + minX), minY);
	    	*/
    	}
    	else {
	    	g2d.drawLine(minX, minY, minX, maxY);
	    	g2d.drawLine(minX, maxY, maxX, maxY);
	    	g2d.drawString("0", minX, maxY + 15);
	    	g2d.drawString(String.format("%.2f", GUI.getInstance().getMaxFeatures().getTimestamp()*0.25), ((maxX-minX)/4) -2 ,maxY + 15);
	    	g2d.drawLine(((maxX-minX)/4) + minX, maxY + 5, ((maxX-minX)/4) + minX, maxY - 5);
	    	g2d.drawString(String.format("%.2f",GUI.getInstance().getMaxFeatures().getTimestamp()*0.5), ((maxX-minX)/2) -2 ,maxY + 15);
	    	g2d.drawLine(((maxX-minX)/2) + minX, maxY + 5, ((maxX-minX)/2) + minX, maxY - 5);
	    	g2d.drawString(String.format("%.2f",GUI.getInstance().getMaxFeatures().getTimestamp()*0.75), (((maxX-minX)/4)*3) -2 ,maxY + 15);
	    	g2d.drawLine(((maxX-minX)/4)*3 + minX, maxY + 5, ((maxX-minX)/4)*3 + minX, maxY - 5);
	    	g2d.drawString(String.format("%.2f",GUI.getInstance().getMaxFeatures().getTimestamp()), maxX, maxY + 15);
    	}
    	    	
    	if(correctTrials != null && incorrectTrials != null){
    		
    		g2d.setStroke(new BasicStroke(2));
    		
	    	for (Trial t : correctTrials){
	    		
	    		if (!t.isFiltered())
	    			t.draw(g2d, Color.orange, minX, maxY, maxX-minX, maxY-minY);
	    	}
	    	
	    	for (Trial t : incorrectTrials){
	    		
	    		if (!t.isFiltered())
	    			t.draw(g2d,new Color(0,150,0), minX, maxY, maxX-minX, maxY-minY);
	    	}
    	}
	}


	public float getStartPhaseCorrect() {
		return startPhaseCorrect;
	}


	public void setStartPhaseCorrect(float startPhaseCorrect) {
		this.startPhaseCorrect = startPhaseCorrect;
	}


	public float getEndPhaseCorrect() {
		return endPhaseCorrect;
	}


	public void setEndPhaseCorrect(float endPhaseCorrect) {
		this.endPhaseCorrect = endPhaseCorrect;
	}


	public float getStartPhaseIncorrect() {
		return startPhaseIncorrect;
	}


	public void setStartPhaseIncorrect(float startPhaseIncorrect) {
		this.startPhaseIncorrect = startPhaseIncorrect;
	}


	public float getEndPhaseIncorrect() {
		return endPhaseIncorrect;
	}


	public void setEndPhaseIncorrect(float endPhaseIncorrect) {
		this.endPhaseIncorrect = endPhaseIncorrect;
	}
}
