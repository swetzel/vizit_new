package view;

import handler.MyActionListener;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import data.FeatureVector;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

import IO.DataWriter;
import java.awt.FlowLayout;


public class Menu extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Menu instance = new Menu();
	private int height = 100;
	private int width = 200;
	private int filterMode = 1; //1 = same, 2 = different, 3 = correct, 4 = incorrect
	private int binMode = 1; // 1 = none, 2 = initialAD, 3 = taskID
	private int grouping = 1; // 1 = task, 2 = student (split at 70% success rate), 3 = group - correct answers, 4 = group - incorrect answers, 5 = group - iPad/PP, 6 = group - PP/iPad
	
	private JLabel labelADMin;
	private JLabel labelADMax;
	private JLabel labelVeloMin;
	private JLabel labelVeloMax;
	private JLabel lblTrailNum;
	private JLabel labelAxisXMin;
	private JLabel labelAxisXMax;
	private JLabel labelAxisYMin;
	private JLabel labelAxisYMax;
	private JLabel labelAxisZMin;
	private JLabel labelAxisZMax;
	private JLabel labelViewportXMin;
	private JLabel labelViewportXMax;
	private JLabel labelViewportYMin;
	private JLabel labelViewportYMax;
	private JLabel labelAccWayMin; 
	private JLabel labelAccWayMax;
	private JLabel labelDeltaXMin;
	private JLabel labelDeltaXMax; 
	private JLabel labelDeltaYMin;
	private JLabel labelDeltaYMax;
	private JLabel labelDeltaZMin;
	private JLabel labelDeltaZMax;
	
	private JCheckBox checkBoxAD;
	private JCheckBox checkBoxVelo;
	private JCheckBox checkBoxAxisX;
	private JCheckBox checkBoxAxisY;
	private JCheckBox checkBoxAxisZ;
	private JCheckBox checkBoxViewportX;
	private JCheckBox checkBoxViewportY;
	private JCheckBox checkBoxAccWay;
	private JCheckBox checkBoxDeltaX;
	private JCheckBox checkBoxDeltaY;
	private JCheckBox checkBoxDeltaZ;
	private JCheckBox chckbxNormalizeTime;
	private JPanel groupPanel;
	
	
	public static Menu getInstance() {
        return instance;
              
    }
	
	private Menu(){

		setLayout(null);
		
		JLabel lblFilter = new JLabel("Grouping");
		lblFilter.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFilter.setBounds(0, 514, 64, 14);
		add(lblFilter);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Task", "Subject", "Group correct", "Group incorrect", "Group iPad/PP", "Group PP/iPad"}));
		comboBox.setBounds(5, 535, 186, 20);
		add(comboBox);
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JComboBox<String> cb = (JComboBox<String>)e.getSource();
		        String filterName = (String)cb.getSelectedItem();
		        Menu.getInstance().setFilterMode(1);
		        
		        if (filterName.equals("Task"))
		        	Menu.getInstance().setGrouping(1);
		        else if (filterName.equals("Subject"))
		        	Menu.getInstance().setGrouping(2);
		        else if (filterName.equals("Group correct"))
		        	Menu.getInstance().setGrouping(3);
		        else if (filterName.equals("Group incorrect"))
		        	Menu.getInstance().setGrouping(4);
		        else if (filterName.equals("Group iPad/PP"))
		        	Menu.getInstance().setGrouping(5);
		        else if (filterName.equals("Group PP/iPad"))
		        	Menu.getInstance().setGrouping(6);		        
			}
		});		
		
		JButton btnCalc = new JButton("Cluster");
		btnCalc.setBounds(5, 578, 89, 23);
		add(btnCalc);
		btnCalc.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				GUI.getInstance().cluster();
			}
		});
		
		JButton btnViz = new JButton("Viz");
		btnViz.setBounds(104, 578, 89, 23);
		add(btnViz);
		btnViz.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
							
				GUI.getInstance().visualizeResults();
			}
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 11, 191, 492);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblMin = new JLabel("Min");
		lblMin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMin.setBounds(66, 0, 46, 14);
		panel.add(lblMin);
		
		JLabel lblMax = new JLabel("Max");
		lblMax.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMax.setBounds(122, 0, 46, 14);
		panel.add(lblMax);
		
		JLabel lblAd = new JLabel("AD");
		lblAd.setBounds(0, 23, 46, 14);
		panel.add(lblAd);
		
		JLabel lblNewLabel = new JLabel("Velocity");
		lblNewLabel.setBounds(0, 48, 46, 14);
		panel.add(lblNewLabel);
		
		 labelADMin = new JLabel("-");
		labelADMin.setBounds(66, 23, 46, 14);
		panel.add(labelADMin);
		
		 labelADMax = new JLabel("-");
		labelADMax.setBounds(122, 23, 46, 14);
		panel.add(labelADMax);
		
		 labelVeloMin = new JLabel("-");
		labelVeloMin.setBounds(66, 48, 46, 14);
		panel.add(labelVeloMin);
		
		 labelVeloMax = new JLabel("-");
		labelVeloMax.setBounds(122, 48, 46, 14);
		panel.add(labelVeloMax);
		
		JLabel lblTrials = new JLabel("Trials");
		lblTrials.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTrials.setBounds(0, 328, 46, 14);
		panel.add(lblTrials);
		
		lblTrailNum = new JLabel("-");
		lblTrailNum.setBounds(66, 328, 46, 14);
		panel.add(lblTrailNum);
		
		checkBoxAD = new JCheckBox("");
		checkBoxAD.setSelected(true);
		checkBoxAD.setBounds(164, 16, 21, 21);
		panel.add(checkBoxAD);
		
		checkBoxVelo = new JCheckBox("");
		checkBoxVelo.setBounds(164, 39, 21, 23);
		panel.add(checkBoxVelo);
		
		JLabel lblNewLabel_1 = new JLabel("AxisX");
		lblNewLabel_1.setBounds(0, 73, 46, 14);
		panel.add(lblNewLabel_1);
		
		labelAxisXMin = new JLabel("-");
		labelAxisXMin.setBounds(66, 73, 46, 14);
		panel.add(labelAxisXMin);
		
		labelAxisXMax = new JLabel("-");
		labelAxisXMax.setBounds(122, 73, 46, 14);
		panel.add(labelAxisXMax);
		
		JLabel labelAxisy = new JLabel("AxisY");
		labelAxisy.setBounds(0, 98, 46, 14);
		panel.add(labelAxisy);
		
		JLabel labelAxisz = new JLabel("AxisZ");
		labelAxisz.setBounds(0, 123, 46, 14);
		panel.add(labelAxisz);
		
		JLabel labelViewportx = new JLabel("ViewportX");
		labelViewportx.setBounds(0, 148, 56, 14);
		panel.add(labelViewportx);
		
		JLabel labelViewportY = new JLabel("ViewportY");
		labelViewportY.setBounds(0, 173, 56, 14);
		panel.add(labelViewportY);
		
		labelAxisYMin = new JLabel("-");
		labelAxisYMin.setBounds(66, 98, 46, 14);
		panel.add(labelAxisYMin);
		
		labelAxisYMax = new JLabel("-");
		labelAxisYMax.setBounds(122, 98, 46, 14);
		panel.add(labelAxisYMax);
		
		labelAxisZMin = new JLabel("-");
		labelAxisZMin.setBounds(66, 123, 46, 14);
		panel.add(labelAxisZMin);
		
		labelAxisZMax = new JLabel("-");
		labelAxisZMax.setBounds(122, 123, 46, 14);
		panel.add(labelAxisZMax);
		
		labelViewportXMin = new JLabel("-");
		labelViewportXMin.setBounds(66, 148, 46, 14);
		panel.add(labelViewportXMin);
		
		labelViewportXMax = new JLabel("-");
		labelViewportXMax.setBounds(122, 148, 46, 14);
		panel.add(labelViewportXMax);
		
		labelViewportYMin = new JLabel("-");
		labelViewportYMin.setBounds(66, 173, 46, 14);
		panel.add(labelViewportYMin);
		
		checkBoxAxisX = new JCheckBox("");
		checkBoxAxisX.setBounds(164, 64, 21, 23);
		panel.add(checkBoxAxisX);
		
		labelViewportYMax = new JLabel("-");
		labelViewportYMax.setBounds(122, 173, 46, 14);
		panel.add(labelViewportYMax);
		
		checkBoxAxisY = new JCheckBox("");
		checkBoxAxisY.setBounds(164, 89, 21, 23);
		panel.add(checkBoxAxisY);
		
		checkBoxAxisZ = new JCheckBox("");
		checkBoxAxisZ.setBounds(164, 114, 21, 23);
		panel.add(checkBoxAxisZ);
		
		checkBoxViewportX = new JCheckBox("");
		checkBoxViewportX.setBounds(164, 139, 21, 23);
		panel.add(checkBoxViewportX);
		
		checkBoxViewportY = new JCheckBox("");
		checkBoxViewportY.setBounds(164, 164, 21, 23);
		panel.add(checkBoxViewportY);
		
		JLabel lblBins = new JLabel("Bins");
		lblBins.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBins.setBounds(0, 353, 46, 14);
		panel.add(lblBins);
		
		ButtonGroup binning = new ButtonGroup();
		
		JRadioButton rdbtnInitialAd = new JRadioButton("Initial AD");
		rdbtnInitialAd.setBounds(0, 390, 77, 23);
		panel.add(rdbtnInitialAd);
		binning.add(rdbtnInitialAd);
		rdbtnInitialAd.setActionCommand("2");
		rdbtnInitialAd.addActionListener(MyActionListener.getInstance());
		
		JRadioButton rdbtnNone = new JRadioButton("None");
		rdbtnNone.setSelected(true);
		rdbtnNone.setBounds(0, 370, 77, 23);
		panel.add(rdbtnNone);
		binning.add(rdbtnNone);
		rdbtnNone.setActionCommand("1");
		rdbtnNone.addActionListener(MyActionListener.getInstance());
		
		JRadioButton rdbtnTaskId = new JRadioButton("Task ID");
		rdbtnTaskId.setBounds(0, 410, 77, 23);
		panel.add(rdbtnTaskId);
		rdbtnTaskId.setActionCommand("3");
		rdbtnTaskId.addActionListener(MyActionListener.getInstance());
		binning.add(rdbtnTaskId);
		
		JLabel lblAccumway = new JLabel("AccumWay");
		lblAccumway.setBounds(0, 196, 56, 14);
		panel.add(lblAccumway);
		
		labelAccWayMin = new JLabel("-");
		labelAccWayMin.setBounds(66, 198, 46, 14);
		panel.add(labelAccWayMin);
		
		labelAccWayMax = new JLabel("-");
		labelAccWayMax.setBounds(122, 198, 46, 14);
		panel.add(labelAccWayMax);
		
		checkBoxAccWay = new JCheckBox("");
		checkBoxAccWay.setBounds(164, 189, 21, 23);
		panel.add(checkBoxAccWay);
		
		JRadioButton rdbtnSubject = new JRadioButton("Subject");
		rdbtnSubject.setBounds(0, 430, 77, 23);
		panel.add(rdbtnSubject);
		rdbtnSubject.setActionCommand("4");
		binning.add(rdbtnSubject);
		
		JRadioButton rdbtnAnswer = new JRadioButton("Answer");
		rdbtnAnswer.setBounds(0, 450, 77, 23);
		panel.add(rdbtnAnswer);
		rdbtnSubject.addActionListener(MyActionListener.getInstance());
		binning.add(rdbtnAnswer);
		rdbtnAnswer.setActionCommand("5");
		
		JLabel lblDeltax = new JLabel("DeltaX");
		lblDeltax.setBounds(0, 221, 46, 14);
		panel.add(lblDeltax);
		
		JLabel lblDeltay = new JLabel("DeltaY");
		lblDeltay.setBounds(0, 246, 46, 14);
		panel.add(lblDeltay);
		
		JLabel lblDeltaz = new JLabel("DeltaZ");
		lblDeltaz.setBounds(0, 271, 46, 14);
		panel.add(lblDeltaz);
		
		labelDeltaXMin = new JLabel("-");
		labelDeltaXMin.setBounds(66, 223, 46, 14);
		panel.add(labelDeltaXMin);
		
		labelDeltaXMax = new JLabel("-");
		labelDeltaXMax.setBounds(122, 223, 46, 14);
		panel.add(labelDeltaXMax);
		
		labelDeltaYMin = new JLabel("-");
		labelDeltaYMin.setBounds(66, 246, 46, 14);
		panel.add(labelDeltaYMin);
		
		labelDeltaYMax = new JLabel("-");
		labelDeltaYMax.setBounds(122, 246, 46, 14);
		panel.add(labelDeltaYMax);
		
		labelDeltaZMin = new JLabel("-");
		labelDeltaZMin.setBounds(66, 271, 46, 14);
		panel.add(labelDeltaZMin);
		
		labelDeltaZMax = new JLabel("-");
		labelDeltaZMax.setBounds(122, 271, 46, 14);
		panel.add(labelDeltaZMax);
		
		checkBoxDeltaX = new JCheckBox("");
		checkBoxDeltaX.setBounds(164, 212, 21, 23);
		panel.add(checkBoxDeltaX);
		
		checkBoxDeltaY = new JCheckBox("");
		checkBoxDeltaY.setBounds(164, 237, 21, 23);
		panel.add(checkBoxDeltaY);
		
		checkBoxDeltaZ = new JCheckBox("");
		checkBoxDeltaZ.setBounds(164, 262, 26, 23);
		panel.add(checkBoxDeltaZ);
		
		
		
		
		rdbtnAnswer.addActionListener(MyActionListener.getInstance());
		
		JButton btnExportData = new JButton("Export Data");
		btnExportData.setBounds(5, 612, 116, 23);
		add(btnExportData);
		
		chckbxNormalizeTime = new JCheckBox("Normalize Time");
		chckbxNormalizeTime.setSelected(true);
		chckbxNormalizeTime.setBounds(0, 642, 97, 23);
		add(chckbxNormalizeTime);
		
		JLabel lblGroups = new JLabel("Groups");
		lblGroups.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblGroups.setBounds(0, 671, 46, 14);
		add(lblGroups);
		
		groupPanel = new JPanel();
		groupPanel.setBounds(0, 692, 196, 92);
		add(groupPanel);
		groupPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnExportData.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					DataWriter.getInstance().writeData(GUI.getInstance().getTrials());
					//DataWriter.getInstance().writeSubjectData(GUI.getInstance().getSubjects());
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
	}

	public void updateGroupPanel(ArrayList<String> groups){
		
		for (String s: groups){
			
			JCheckBox newCheckbox = new JCheckBox(s);
			groupPanel.add(newCheckbox);
		}
	}
	
	public void setBinMode(int m){
		
		binMode = m;
	}
	
		
	public int getBinMode(){
		return binMode;
	}
	
	public void updateLables(FeatureVector min, FeatureVector max, int num){
		
		labelADMin.setText(""+min.getAngularDisparity());
		labelADMax.setText(""+max.getAngularDisparity());
		labelVeloMin.setText(""+min.getVelocity());
		labelVeloMax.setText(""+max.getVelocity());
		labelAxisXMin.setText(""+min.getEulerX());
		labelAxisXMax.setText(""+max.getEulerX());
		labelAxisYMin.setText(""+min.getEulerY());
		labelAxisYMax.setText(""+max.getEulerY());
		labelAxisZMin.setText(""+min.getEulerZ());
		labelAxisZMax.setText(""+max.getEulerZ());
		labelViewportXMin.setText(""+min.getViewportX());
		labelViewportXMax.setText(""+max.getViewportX());
		labelViewportYMin.setText(""+min.getViewportY());
		labelViewportYMax.setText(""+max.getViewportY());
		labelAccWayMin.setText(""+min.getAccumulatedWay());
		labelAccWayMax.setText(""+max.getAccumulatedWay());
		labelDeltaXMin.setText(""+min.getEulerXDelta());
		labelDeltaXMax.setText(""+max.getEulerXDelta());
		labelDeltaYMin.setText(""+min.getEulerYDelta());
		labelDeltaYMax.setText(""+max.getEulerYDelta());
		labelDeltaZMin.setText(""+min.getEulerZDelta());
		labelDeltaZMax.setText(""+max.getEulerZDelta());
		
		
		lblTrailNum.setText(""+num);
		
	}
	
	public JCheckBox getCheckBoxNormalize() {
		return chckbxNormalizeTime;
	}
	
	public JCheckBox getCheckBoxAD() {
		return checkBoxAD;
	}
	
	public JCheckBox getCheckNormalize() {
		return chckbxNormalizeTime;
	}


	public void setCheckBoxAD(JCheckBox checkBoxAD) {
		this.checkBoxAD = checkBoxAD;
	}

	public JCheckBox getCheckBoxVelo() {
		return checkBoxVelo;
	}

	public void setCheckBoxVelo(JCheckBox checkBoxVelo) {
		this.checkBoxVelo = checkBoxVelo;
	}

	public void resize(int width, int height){
		
		this.height = height;
		setBounds(width - this.width, 0, this.width, this.height);
	}
	
	

	public int getFilterMode() {
		return filterMode;
	}

	public void setFilterMode(int filterMode) {
		this.filterMode = filterMode;
	}

	public JCheckBox getCheckBoxAxisX() {
		return checkBoxAxisX;
	}

	public void setCheckBoxAxisX(JCheckBox checkBoxAxisX) {
		this.checkBoxAxisX = checkBoxAxisX;
	}

	public JCheckBox getCheckBoxAxisY() {
		return checkBoxAxisY;
	}

	public void setCheckBoxAxisY(JCheckBox checkBoxAxisY) {
		this.checkBoxAxisY = checkBoxAxisY;
	}

	public JCheckBox getCheckBoxAxisZ() {
		return checkBoxAxisZ;
	}

	public void setCheckBoxAxisZ(JCheckBox checkBoxAxisZ) {
		this.checkBoxAxisZ = checkBoxAxisZ;
	}

	public JCheckBox getCheckBoxViewportX() {
		return checkBoxViewportX;
	}

	public void setCheckBoxViewportX(JCheckBox checkBoxViewportX) {
		this.checkBoxViewportX = checkBoxViewportX;
	}

	public JCheckBox getCheckBoxViewportY() {
		return checkBoxViewportY;
	}

	public void setCheckBoxViewportY(JCheckBox checkBoxViewportY) {
		this.checkBoxViewportY = checkBoxViewportY;
	}

	public JCheckBox getCheckBoxAccWay() {
		return checkBoxAccWay;
	}

	public void setCheckBoxAccWay(JCheckBox checkBoxAccWay) {
		this.checkBoxAccWay = checkBoxAccWay;
	}

	public JCheckBox getCheckBoxDeltaX() {
		return checkBoxDeltaX;
	}

	public JCheckBox getCheckBoxDeltaY() {
		return checkBoxDeltaY;
	}

	public JCheckBox getCheckBoxDeltaZ() {
		return checkBoxDeltaZ;
	}

	public int getGrouping() {
		return grouping;
	}

	public void setGrouping(int grouping) {
		this.grouping = grouping;
	}
}
