package view;

import java.awt.Dimension;

import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import analysis.Clustering;
import IO.Parser;
import data.FeatureVector;
import data.Subject;
import data.Trial;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Vector;


public class GUI extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static GUI instance = new GUI();
	public final static JFileChooser fc = new JFileChooser();
	private JMenuBar menuBar;
	private JMenu menuFile;
	private JMenuItem load;
	private JDesktopPane desktop;
	private ArrayList<Trial> trials = new ArrayList<Trial>();
	private Map<String, Subject> subjects = new HashMap<String, Subject>();
	private FeatureVector minFeatures;
	private FeatureVector maxFeatures;
	private ImageFrame frame;
	private ArrayList<String> groups = new ArrayList<String>();
	
	public static GUI getInstance() {
        return instance;
    }
	
	private GUI(){
		
		getContentPane().setBackground(Color.WHITE);
		
		this.setTitle("Viz it!");
		
		menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		 
		menuFile = new JMenu("File");
		menuBar.add(menuFile);
		
		load = new JMenuItem("Load Data");
		menuFile.add(load);
		
		desktop = new JDesktopPane();
		desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		desktop.setBackground(Color.LIGHT_GRAY);
		
		this.getContentPane().setPreferredSize(new Dimension(1200, 800));
		this.pack();
		
		Menu.getInstance().resize(this.getContentPane().getWidth(), this.getContentPane().getHeight());
			
		getContentPane().setLayout(null);
		getContentPane().add(Menu.getInstance());
		getContentPane().add(desktop);
		
		// handle resize
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				Menu.getInstance().resize(GUI.this.getContentPane().getWidth(), GUI.this.getContentPane().getHeight());
				desktop.setMaximumSize(new Dimension(GUI.this.getContentPane().getWidth()-Menu.getInstance().getWidth(), GUI.this.getContentPane().getHeight()));
				desktop.setSize(GUI.this.getContentPane().getWidth()-Menu.getInstance().getWidth(), GUI.this.getContentPane().getHeight());
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				
			}
		});	
		
		load.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fc.showOpenDialog(GUI.getInstance());
				
				if (returnVal == JFileChooser.APPROVE_OPTION) { 
					
					File file = fc.getSelectedFile();
					
					try {
						trials = Parser.getInstance().readFolder(file.getAbsolutePath(), "default group", new ArrayList<Trial>());
						
						for (String group : groups)
							System.out.println("groups: " +group);
						
						calcAccumWay();
						calcDrags();
						calcSubjects();
						visualizeResults();
						Menu.getInstance().updateGroupPanel(groups);
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				else {
					System.out.println("Open command cancelled by user.");
		        }
			}
		});
		
		frame = new ImageFrame(trials);
		frame.setVisible(true); 
		desktop.add(frame);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	
		
	}
	
	public void addGroupID(String id){
		groups.add(id);
	}
	
	public ArrayList<String> getGroupIDs(){
		return groups;
	}
	
	private void calcSubjects() {
		
		// sort by subject
		HashMap<String, ArrayList<Trial>> subjectList = new HashMap<String, ArrayList<Trial>>();		
		
		for (Trial t : trials){
			
			if (subjectList.containsKey(t.getSubject())){
				subjectList.get(t.getSubject()).add(t);
			}
			else{
				
				ArrayList<Trial> newTrials = new ArrayList<Trial>();
				newTrials.add(t);
				subjectList.put(t.getSubject(), newTrials);
			}
				
		}
				
		for (Map.Entry<String, ArrayList<Trial>> entry : subjectList.entrySet())
		{
			int correctAnswers = 0;
			float time = 0; 
			ArrayList<Integer> answers = new ArrayList<Integer>();
			String id = null;
			
			for (Trial t : entry.getValue()){
				
				id = t.getGroupID();
				time += t.getDuration();
				if(t.isCorrectAnswer()){
					correctAnswers++;
					answers.add(1);
				}
				else
					answers.add(0);
			}
			
			
			subjects.put(entry.getKey(),new Subject(entry.getKey(), correctAnswers, entry.getValue().size(), time,answers,id));
		}
		
		for (Trial t : trials){
			
			t.setSuccessRate(subjects.get(t.getSubject()).getSuccessRate());
		}
		
	}
	
	public ArrayList<Subject> getSubjects(){
		
		ArrayList<Subject> subs = new ArrayList<Subject>();
		
		for (Map.Entry<String,Subject> subjectList : subjects.entrySet()){
			
			subs.add(subjectList.getValue());
		}
		
		return subs;
	}

	private void calcDrags() {

		for (Trial t: trials){
			t.calcDrags();
		}
	}
	
	public void calcStartEndPhases(ArrayList<Trial> correct, ArrayList<Trial> incorrect){

		float startCorr = 0;
		float startIncorr = 0;
		float endCorr = 0;
		float endIncorr = 0;
		
		for (Trial t: correct){
			t.findStartOfPhysicalPhase();
			startCorr += t.getPhysicalPhaseStartRelative();
		}
		
		for (Trial t: incorrect){
			t.findStartOfPhysicalPhase();
			startIncorr += t.getPhysicalPhaseStartRelative();
		}
		
		for (Trial t: correct){
			t.findEndOfPhysicalPhase();
			endCorr += t.getPhysicalPhaseEndRelative();
		}
		
		for (Trial t: incorrect){
			t.findEndOfPhysicalPhase();
			endIncorr += t.getPhysicalPhaseEndRelative();
		}


		frame.setPhases(startCorr/correct.size(),startIncorr/incorrect.size(), endCorr/correct.size(),endIncorr/incorrect.size());
		
		//System.out.println(start/correct.size());
	}
	
	private void calcAccumWay(){
		
		
		for (Trial t: trials)
			t.calcAccumWay();
	}
	
	public ArrayList<Trial> getTrials(){
		return trials;
	}
	
	
	public void findMinMaxFeatures(ArrayList<Trial> trials){
		
		float minTime = 10000;
		float maxTime = -10000;
		float minD1 = 10000;
		float maxD1 = -10000;
		float minD2 = 10000;
		float maxD2 = -10000;
		float minD3 = 10000;
		float maxD3 = -10000;
		float minD4 = 10000;
		float maxD4 = -10000;
		float minD5 = 10000;
		float maxD5 = -10000;
		float minD6 = 10000;
		float maxD6 = -10000;
		float minD7 = 10000;
		float maxD7 = -10000;
		float minD8 = 10000;
		float maxD8 = -10000;
		float minD9 = 10000;
		float maxD9 = -10000;
		float minD10 = 10000;
		float maxD10 = -10000;
		float minD11 = 10000;
		float maxD11 = -10000;
		float minD12 = 10000;
		float maxD12 = -10000;
		float minD13 = 10000;
		float maxD13 = -10000;
		
		for (Trial t : trials){
			
			if (t.isSameTrial()){
			
				t.findMinMaxFeatures();
				
				minTime = Math.min(minTime, t.getMinFeatures().getTimestamp());
				maxTime = Math.max(maxTime, t.getMaxFeatures().getTimestamp());
				minD1 = Math.min(minD1, t.getMinFeatures().getVelocity());
				maxD1 = Math.max(maxD1, t.getMaxFeatures().getVelocity());
				minD2 = Math.min(minD2, t.getMinFeatures().getAngularDisparity());
				maxD2 = Math.max(maxD2, t.getMaxFeatures().getAngularDisparity());
				minD3 = Math.min(minD3, t.getMinFeatures().getEulerX());
				maxD3 = Math.max(maxD3, t.getMaxFeatures().getEulerX());
				minD4 = Math.min(minD4, t.getMinFeatures().getEulerY());
				maxD4 = Math.max(maxD4, t.getMaxFeatures().getEulerY());
				minD5 = Math.min(minD5, t.getMinFeatures().getEulerZ());
				maxD5 = Math.max(maxD5, t.getMaxFeatures().getEulerZ());
				minD6 = Math.min(minD6, t.getMinFeatures().getVisibleArea());
				maxD6 = Math.max(maxD6, t.getMaxFeatures().getVisibleArea());
				minD7 = Math.min(minD7, t.getMinFeatures().getVisibleAreaAccumulated());
				maxD7 = Math.max(maxD7, t.getMaxFeatures().getVisibleAreaAccumulated());
				minD8 = Math.min(minD8, t.getMinFeatures().getViewportX());
				maxD8 = Math.max(maxD8, t.getMaxFeatures().getViewportX());
				minD9 = Math.min(minD9, t.getMinFeatures().getViewportY());
				maxD9 = Math.max(maxD9, t.getMaxFeatures().getViewportY());
				minD10 = Math.min(minD10, t.getMinFeatures().getAccumulatedWay());
				maxD10 = Math.max(maxD10, t.getMaxFeatures().getAccumulatedWay());
				minD11 = Math.min(minD11, t.getMinFeatures().getEulerXDelta());
				maxD11 = Math.max(maxD11, t.getMaxFeatures().getEulerXDelta());
				minD12 = Math.min(minD12, t.getMinFeatures().getEulerYDelta());
				maxD12 = Math.max(maxD12, t.getMaxFeatures().getEulerYDelta());
				minD13 = Math.min(minD13, t.getMinFeatures().getEulerZDelta());
				maxD13 = Math.max(maxD13, t.getMaxFeatures().getEulerZDelta());
			}
		}
		
		//minFeatures = new FeatureVector(minTime, minD1, minD2, minD3, minD4, minD5, minD6, minD7, minD8, minD9);
		//maxFeatures = new FeatureVector(maxTime, maxD1, maxD2, maxD3, maxD4, maxD5, maxD6, maxD7, maxD8, maxD9);
		minFeatures = new FeatureVector(minTime, minD1, 0, -1, -1, -1,0,0,0, minD6, minD7, 0, minD9, minD10);
		maxFeatures = new FeatureVector(maxTime, maxD1, 180, 1, 1, 1,1,1,1, maxD6, maxD7, 2, maxD9, maxD10);
	}
	
	
	
	
	
	public void clusterData(ArrayList<Trial> trialsToCluster){
							
			Vector<FeatureVector> data = new Vector<FeatureVector>();	
			
			for (Trial t : trialsToCluster){
				
				data.addAll(t.getData());
			}
			
			HashMap<FeatureVector, Integer> mapping = Clustering.getInstance().kMeans(2, data);

			for (Trial t : trials){
				t.clearCluster();	
			}
			
			for (Trial t : trials){
			
				for (FeatureVector fv : t.getData()){
				
					if (mapping.containsKey(fv)){
					
						int clusterID = mapping.get(fv);
						fv.setClusterID(clusterID);
					}
				}
				
			}
		
		
	}

	public ArrayList<Trial> binTrialsByTask(ArrayList<Trial> data){
		
		
		/// sort trials by task id
		HashMap<String, ArrayList<Trial>> tasks = new HashMap<String, ArrayList<Trial>>();
		
		for (Trial t : data){
			
			if (tasks.containsKey(t.getTaskID())){
				tasks.get(t.getTaskID()).add(t);
			}
			else{
				
				ArrayList<Trial> trials = new ArrayList<Trial>();
				trials.add(t);
				tasks.put(t.getTaskID(), trials);
			}
				
		}
		
		//find max timestamp
		int maxTime = 0;
		ArrayList<Trial> binnedTrials = new ArrayList<Trial>();

		for (Map.Entry<String, ArrayList<Trial>> entry : tasks.entrySet())
		{
			
			for (Trial t : entry.getValue())	
				maxTime = Math.max(t.getData().size(), maxTime);
			for (Trial t : entry.getValue())
				t.setIndex((float)t.getData().size()/maxTime); 
			
			binnedTrials.add(bin(entry.getValue(), maxTime));
			
		}
		
		return binnedTrials;
	}
	
	public ArrayList<Trial> binTrialsBySubject(ArrayList<Trial> data){
		
		
		/// sort trials by task subject
		HashMap<String, ArrayList<Trial>> tasks = new HashMap<String, ArrayList<Trial>>();
		
		for (Trial t : data){
			
			if (tasks.containsKey(t.getSubject())){
				tasks.get(t.getSubject()).add(t);
			}
			else{
				
				ArrayList<Trial> trials = new ArrayList<Trial>();
				trials.add(t);
				tasks.put(t.getSubject(), trials);
			}
				
		}
		
		//find max timestamp
		int maxTime = 0;
		ArrayList<Trial> binnedTrials = new ArrayList<Trial>();

		for (Map.Entry<String, ArrayList<Trial>> entry : tasks.entrySet())
		{
			
			for (Trial t : entry.getValue())	
				maxTime = Math.max(t.getData().size(), maxTime);
			for (Trial t : entry.getValue())
				t.setIndex((float)t.getData().size()/maxTime); 
			
			binnedTrials.add(bin(entry.getValue(), maxTime));
			
		}
		
		return binnedTrials;
	}
	
	private ArrayList<Trial> binTrialsByAnswer(ArrayList<Trial> data) {
		
		//find max timestamp
		int maxTime = 0;
		ArrayList<Trial> binnedTrials = new ArrayList<Trial>();

			
		for (Trial t : data)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : data)
			t.setIndex((float)t.getData().size()/maxTime); 
		
		binnedTrials.add(bin(data, maxTime));
			
		return binnedTrials;
	}
	
	public ArrayList<Trial> binTrialsByAD(ArrayList<Trial> data){
		
		ArrayList<Trial> trials = new ArrayList<Trial>();
		
		ArrayList<Trial> trials30 = new ArrayList<Trial>();
		ArrayList<Trial> trials60 = new ArrayList<Trial>();
		ArrayList<Trial> trials90 = new ArrayList<Trial>();
		ArrayList<Trial> trials120 = new ArrayList<Trial>();
		ArrayList<Trial> trials150 = new ArrayList<Trial>();
		ArrayList<Trial> trials180 = new ArrayList<Trial>();
		
		int maxTime = 0;
		// sort trials by initial ad
		
		for (Trial t : data){
			
			if (t.getInitialAD() <= 30)
				trials30.add(t);
			if (t.getInitialAD() > 30 && t.getInitialAD() <= 60)
				trials60.add(t);
			if (t.getInitialAD() > 60 && t.getInitialAD() <= 90)
				trials90.add(t);
			if (t.getInitialAD() > 90 && t.getInitialAD() <= 120)
				trials120.add(t);
			if (t.getInitialAD() > 120 && t.getInitialAD() <= 150)
				trials150.add(t);
			if (t.getInitialAD() > 150)
				trials180.add(t);
	
		}
		
		for (Trial t : trials30)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : trials30)
			t.setIndex((float)t.getData().size()/maxTime); 
		if (!trials30.isEmpty())
			trials.add(bin(trials30, maxTime));
		
		maxTime = 0;
		for (Trial t : trials60)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : trials60)
			t.setIndex((float)t.getData().size()/maxTime); 
		if (!trials60.isEmpty())
			trials.add(bin(trials60, maxTime));	
		
		maxTime = 0;
		for (Trial t : trials90)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : trials90)
			t.setIndex((float)t.getData().size()/maxTime); 
		if (!trials90.isEmpty())
			trials.add(bin(trials90, maxTime));	
		
		maxTime = 0;
		for (Trial t : trials120)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : trials120)
			t.setIndex((float)t.getData().size()/maxTime); 
		if (!trials120.isEmpty())
			trials.add(bin(trials120, maxTime));	
		
		maxTime = 0;
		for (Trial t : trials150)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : trials150)
			t.setIndex((float)t.getData().size()/maxTime); 
		if (!trials150.isEmpty())
			trials.add(bin(trials150, maxTime));	
		
		maxTime = 0;
		for (Trial t : trials180)	
			maxTime = Math.max(t.getData().size(), maxTime);
		for (Trial t : trials180)
			t.setIndex((float)t.getData().size()/maxTime); 
		if (!trials180.isEmpty())
			trials.add(bin(trials180, maxTime));	
		
		System.out.println("30 " + trials30.size());
		System.out.println("60 " + trials60.size());
		System.out.println("90 " + trials90.size());
		System.out.println("120 " + trials120.size());
		System.out.println("150 " + trials150.size());
		System.out.println("180 " + trials180.size());
		return trials;
	}
	
	public Trial bin(ArrayList<Trial> data, int maxTime){
		
		Trial binTrial = new Trial(data.get(0).getSubject(), 
								   data.get(0).getTaskID(), 
								   0, 
								   data.get(0).isSameTrial(),  
								   data.get(0).getCondition(),  
								   data.get(0).isResponse(),  
								   data.get(0).getTrialBlock(), 
								   data.get(0).getTaskNumber(), 
								   data.get(0).getTaskPresented() ,
								   data.get(0).isCorrectAnswer());
				
		for (int i = 0; i < maxTime; ++i){
			
			float sumAD = 0;
			float sumVelo = 0;
			float sumEX = 0;
			float sumEY = 0;
			float sumEZ = 0;
			float sumEXDelta = 0;
			float sumEYDelta = 0;
			float sumEZDelta = 0;
			float sumVX = 0;
			float sumVY = 0;
			float sumAccWay = 0;
			
			for (Trial t : data){
				
				int index = (int) Math.floor(i*t.getIndex());
				sumAD += t.getData().get(index).getAngularDisparity(); 
				sumVelo += t.getData().get(index).getVelocity(); 
				sumEX += t.getData().get(index).getEulerX(); 
				sumEY += t.getData().get(index).getEulerY(); 
				sumEZ += t.getData().get(index).getEulerZ(); 
				sumEXDelta += t.getData().get(index).getEulerXDelta();
				sumEYDelta += t.getData().get(index).getEulerYDelta();
				sumEZDelta += t.getData().get(index).getEulerZDelta();
				sumVX += t.getData().get(index).getViewportX(); 
				sumVY += t.getData().get(index).getViewportY(); 
				sumAccWay += t.getData().get(index).getAccumulatedWay(); 
				
			}
			
			binTrial.addData(new FeatureVector((float)(i*0.05), sumVelo/data.size(), sumAD/data.size(), sumEX/data.size(), sumEY/data.size(), sumEZ/data.size(),sumEXDelta/data.size(),sumEYDelta/data.size(),sumEZDelta/data.size(), 0, 0, sumVX/data.size(), sumVY/data.size(), sumAccWay/data.size()));
		}
		
		return binTrial;
	}
	
	
	public void visualizeResults(){
		
		for (Trial t : trials){
			t.setFiltered(true);
		}
		
		findMinMaxFeatures(trials);
		ArrayList<Trial> activeTrialsCorrect = filterTrials(true);
		ArrayList<Trial> activeTrialsIncorrect = filterTrials(false);
		calcStartEndPhases(activeTrialsCorrect, activeTrialsIncorrect);
		
		
		if (Menu.getInstance().getBinMode() == 2){
			activeTrialsCorrect = binTrialsByAD(activeTrialsCorrect);
			activeTrialsIncorrect = binTrialsByAD(activeTrialsIncorrect);
		}
		if (Menu.getInstance().getBinMode() == 3){
			activeTrialsCorrect = binTrialsByTask(activeTrialsCorrect);
			activeTrialsIncorrect = binTrialsByTask(activeTrialsIncorrect);
		}
		if (Menu.getInstance().getBinMode() == 4){
			activeTrialsCorrect = binTrialsBySubject(activeTrialsCorrect);
			activeTrialsIncorrect = binTrialsBySubject(activeTrialsIncorrect);
		}
		if (Menu.getInstance().getBinMode() == 5){
			activeTrialsCorrect = binTrialsByAnswer(activeTrialsCorrect);
			activeTrialsIncorrect = binTrialsByAnswer(activeTrialsIncorrect);
		}
		
		for (Trial t : activeTrialsCorrect){
			t.findMinMaxFeatures();
			t.setMinMaxFeatures(minFeatures, maxFeatures);
		}
		
		for (Trial t : activeTrialsIncorrect){
			t.findMinMaxFeatures();
			t.setMinMaxFeatures(minFeatures, maxFeatures);
		}

		frame.setTrials(activeTrialsCorrect, activeTrialsIncorrect);
		Menu.getInstance().updateLables(minFeatures, maxFeatures, activeTrialsCorrect.size()+activeTrialsIncorrect.size());
	}

	public ArrayList<Trial> filterTrials(boolean correctAnswers){
		
		int group = Menu.getInstance().getGrouping();
		
		ArrayList<Trial> activeTrials = new ArrayList<Trial>();
		
	
		for (Trial t : trials){
			
			switch (group) {
			case 1:
				if (t.isSameTrial() && t.isCorrectAnswer() == correctAnswers && t.getDrags().size() > 0){
					activeTrials.add(t);
					t.setFiltered(false);
				}
				break;
			case 2:
				
				if (correctAnswers)
				{
					if (subjects.get(t.getSubject()).getSuccessRate() > 0.74 && t.getDrags().size() > 0){

						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				else {
					if (subjects.get(t.getSubject()).getSuccessRate() <= 0.74 && t.getDrags().size() > 0){
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				break;
			case 3: // only correct answers
				if (correctAnswers) // iPad-PP
				{
					if (t.getGroupID().equals("iPad-PP") && t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				else {
					if (t.getGroupID().equals("PP-iPad") &&  t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				
				break;
				
			case 4: // only incorrect answers
				if (correctAnswers) // iPad-PP
				{
					if (t.getGroupID().equals("iPad-PP") && !t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				else {
					if (t.getGroupID().equals("PP-iPad") &&  !t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				
				break;
			case 5: // only group iPad/PP
				if (correctAnswers) 
				{
					if (t.getGroupID().equals("iPad-PP") && t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				else {
					if (t.getGroupID().equals("iPad-PP") &&  !t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				
				break;
			case 6: // only group PP/iPad
				if (correctAnswers) 
				{
					if (t.getGroupID().equals("PP-iPad") && t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				else {
					if (t.getGroupID().equals("PP-iPad") &&  !t.isCorrectAnswer() && t.getDrags().size() > 0){
						
						
						activeTrials.add(t);
						t.setFiltered(false);
					}
				}
				
			}
			
			
		}
		System.out.println("trials:" + correctAnswers + " " +activeTrials.size());
		return activeTrials;
	}
	
	public void cluster(){
		
		
		for (Trial t : trials){
			t.setFiltered(true);
		}
		
		findMinMaxFeatures(trials);
		ArrayList<Trial> activeTrialsGreen = filterTrials(true);
		ArrayList<Trial> activeTrialsRed = filterTrials(false);
		
		
		
		if (Menu.getInstance().getBinMode() == 2){
			activeTrialsGreen = binTrialsByAD(activeTrialsGreen);
			activeTrialsRed = binTrialsByAD(activeTrialsRed);
		}
		else if (Menu.getInstance().getBinMode() == 3){
			activeTrialsGreen = binTrialsByTask(activeTrialsGreen);
			activeTrialsRed = binTrialsByTask(activeTrialsRed);
		}
		
		for (Trial t : activeTrialsGreen){
			t.findMinMaxFeatures();
			t.setMinMaxFeatures(minFeatures, maxFeatures);
		}
		
		for (Trial t : activeTrialsRed){
			t.findMinMaxFeatures();
			t.setMinMaxFeatures(minFeatures, maxFeatures);
		}
		
		//clusterData(activeTrialsIncorrect);
	}
	
	public FeatureVector getMinFeatures() {
		return minFeatures;
	}

	public FeatureVector getMaxFeatures() {
		return maxFeatures;
	}

	
	
}
