package view;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;

import data.FeatureVector;
import data.Trial;


public class ImageFrame extends JInternalFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ImagePanel panel;
	
	
	
	public ImageFrame(ArrayList<Trial> trials){
		
		super(Menu.getInstance().getFilterMode()+"",
		          true, //resizable
		          true, //closable
		          true, //maximizable
		          true);//iconifiable
		 try {
			setSelected(true);
		} catch (PropertyVetoException e) {
			
			e.printStackTrace();
		}
		
		//this.setMaximumSize(new Dimension(maxWidth,maxHeight));
		this.setSize(new Dimension(800, 600));
		setLocation(10,10);
		getContentPane().setLayout(null);    
		moveToFront(); 
		getContentPane().setSize(this.getWidth(),  this.getHeight());
		 
		panel = new ImagePanel();
		
		getContentPane().add(panel);
		
		panel.setBounds(0, 0, this.getContentPane().getWidth(), this.getContentPane().getHeight());
		panel.resize(this.getContentPane().getWidth(), this.getContentPane().getHeight());
		
			
		this.addComponentListener(new ComponentListener() {
				
			@Override
			public void componentShown(ComponentEvent e) {
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				//resizeElements();
				panel.setBounds(0, 0, ImageFrame.this.getContentPane().getWidth(), ImageFrame.this.getContentPane().getHeight());
				panel.resize(ImageFrame.this.getContentPane().getWidth(), ImageFrame.this.getContentPane().getHeight());
				panel.repaint();
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				
			}
		});
	}
	
	public void setPhases(float startCorrect, float startIncorrect, float endCorrect, float endIncorrect){
		
		panel.setStartPhaseCorrect(startCorrect);
		panel.setStartPhaseIncorrect(startIncorrect);
		panel.setEndPhaseCorrect(endCorrect);
		panel.setEndPhaseIncorrect(endIncorrect);
		
	}
	
	public void setTrials(ArrayList<Trial> correctTrials, ArrayList<Trial> incorrectTrials){
		
		panel.setTrials(correctTrials, incorrectTrials);
	}
}
