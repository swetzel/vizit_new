package handler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.GUI;
import view.Menu;

public class MyActionListener implements ActionListener {

	private static MyActionListener instance = new MyActionListener();
	
	public static MyActionListener getInstance() {
        return instance;
    }
	
	private MyActionListener() {
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Menu.getInstance().setBinMode(Integer.parseInt(e.getActionCommand()));
	}

}
