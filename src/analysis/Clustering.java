package analysis;

import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

import data.FeatureVector;


public class Clustering {

	
	private static Clustering instance = new Clustering();
	//public int epsilon = 40; 
	//public int minPts = 3;
	//public int timeDistance = 40;
	//public boolean useTime = false;
	
	private Clustering(){}
	
	public static Clustering getInstance() {
        return instance;
    }
		
	
	/**
	 * 
	 * @param gazes
	 * @param epsilon
	 * @param minPts
	 * @return
	 */
	/*
	public void dbScan(Vector<FeatureVector> gazes, int epsilon, int minPts){
		
		
		// reset points
		ArrayList<Gaze> toCheck = new ArrayList<>();
		for (Gaze g: gazes){
			g.visited = Gaze.Visited.UNCLASSIFIED;
			g.clusterID = -1;
		}
		toCheck.addAll(gazes);
		
		int n = gazes.size();
		int cluster = 0;
		// now scan the points in the data-set point by point 
		for (int i = 0; i < n; i++) {
			Gaze p = gazes.get(i);
			if (p.visited == Gaze.Visited.UNCLASSIFIED) {
				
				// get the nearest points to the point 'p'
				// within the range defined by radius 'epsilon'
				ArrayList<Gaze> nearest = regionQuery(toCheck, p, epsilon);
				if (nearest.size() <= minPts) {
					p.visited = Gaze.Visited.NOISE;
				} else {
					
					// the point 'p' has sufficient number of neighbors 
					// to form its own cluster, now we need to expand that cluster
					// to include the points that are directly-reachable by
					// the points in the neighborhood of 'p'
					
					cluster++;
					p.clusterID = cluster;
					p.visited = Gaze.Visited.VISITED;
					expandCluster(cluster,nearest, toCheck, epsilon, minPts);
				}
			}
		}
		
		ArrayList<Clustering> allCluster = new ArrayList<Clustering>();
		
		for (int i = 0; i < cluster; ++i){
			allCluster.add(new Clustering());
		}
		
		
		for (Gaze g: gazes){
			if (g.clusterID != -1)
				allCluster.get(g.clusterID-1).add(g);
		}
		
				
	}
	
	private void expandCluster(int id, ArrayList<Gaze> neighbors, ArrayList<Gaze> toCheck, int eps, int minPts){
		
		
		while (!neighbors.isEmpty()){
			Gaze currentGaze = neighbors.remove(0);
			
			if (currentGaze.visited == Gaze.Visited.UNCLASSIFIED ){
				
				ArrayList<Gaze> newNeighbors = regionQuery(toCheck, currentGaze, eps);
				if (newNeighbors.size() >= minPts){
					neighbors.addAll(newNeighbors);
				}
				currentGaze.clusterID = id;
				currentGaze.visited = Gaze.Visited.VISITED;
			}
			if (currentGaze.visited == Gaze.Visited.NOISE){
			
				currentGaze.clusterID = id;
			}
		}
	}
	
	private ArrayList<Gaze> regionQuery(ArrayList<Gaze> toCheck,Gaze g, int eps){
		
		ArrayList<Gaze> gazes = new ArrayList<Gaze>(); 
		
		for (Gaze gaze: toCheck ){
			
			
			if (g.getDistance(gaze) <= eps){
				gazes.add(gaze);
			}
			
		}
		
		return gazes;
	
	}
*/
	/**
	 * 
	 * @param clusterNumber
	 * @param gazes
	 * @return
	 */
	public HashMap<FeatureVector, Integer > kMeans(int clusterNumber, Vector<FeatureVector> data){
				
		int iterations = 100;
		
		//Vector<Point2D.Double> centroids = new Vector<Point2D.Double>();
		Vector<FeatureVector> centroids = new Vector<FeatureVector>();
		Vector<Vector<FeatureVector>> clusters = new Vector<Vector<FeatureVector>>();
		
		// generate output vectors
		for (int i = 0; i < clusterNumber; ++i){
			
			clusters.add(new Vector<FeatureVector>());
			//clusters.add(new Cluster());
		}
				
		// pick random centroids
		Random rn = new Random();
		for (int i = 0; i < clusterNumber; ++i){
			
			FeatureVector vec = data.get(rn.nextInt(data.size()));
			centroids.add(vec);
		}
		
		// assign points to clusters
		for (int i = 0; i < iterations; ++i){
			
			//clear output 
			for (int j = 0; j < clusterNumber; ++j){
				clusters.get(j).clear();
			}

			int cluster = 0;
			for (FeatureVector vec : data){
				double distance = 1000.0;
				for (int j = 0; j < clusterNumber; ++j){
					
					
											 
					double newDistance = Math.sqrt((Math.pow((centroids.get(j).getVelocity() - vec.getVelocity()),2) + 
													Math.pow((centroids.get(j).getAngularDisparity() - vec.getAngularDisparity()),2)));
							
					if (newDistance < distance){
						distance = newDistance;
						cluster = j;
					}
				}
				vec.setClusterID(cluster);
				clusters.get(cluster).add(vec);
			}
			
			// update centroids
			int currentCluster = 0;
			for (Vector<FeatureVector> v : clusters){
			
				float sumD1 = 0;
				float sumD2 = 0;
				
				for (FeatureVector vec : v){
					
					sumD1 += vec.getVelocity();
					sumD2 += vec.getAngularDisparity();
				}
				
				centroids.get(currentCluster).setVelocity(sumD1/v.size());
				centroids.get(currentCluster).setAngularDisparity(sumD2/v.size());
				currentCluster++;
			}
		}
		
		HashMap<FeatureVector, Integer> clusterMapping = new HashMap<FeatureVector, Integer>();
		
		for (Vector<FeatureVector> v : clusters){
			
			for (FeatureVector vec : v){
				
				clusterMapping.put(vec, vec.getClusterID());
			}
		}
		return clusterMapping;
		
	}
		
	
	
	
	/*
	private Vector<Gaze> gazes = new Vector<Gaze>();
	private ConvexHull hull = new ConvexHull();
	
	public Cluster(Vector<Gaze> gazes){
		this.gazes = gazes;
	}
	
	public Cluster(){
		
	}
	
	public void clear(){
		gazes.clear();
	}
	
	public void add(Gaze g){
		gazes.add(g);
	}
	
	public Vector<Gaze> getGazes(){
		return gazes;
	}
	
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		for (Gaze g : gazes){
			g2d.fillRect((int) (g.getX() * scaleX),(int) (g.getY() * scaleY),2, 2);	
		}
		//System.out.println(gazes.size());
		hull.calculateConvexHull(this);
		hull.draw(g2d, scaleX, scaleY);
	}
	*/
}
