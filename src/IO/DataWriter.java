
package IO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import data.Subject;
import data.Trial;



/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 * 
 */
public class DataWriter {

	
	private static DataWriter instance = new DataWriter();
	
	
	private DataWriter(){}
	
	
	public static DataWriter getInstance() {
        return instance;
    }
	
	/**
	 * Transforms float to String and replaces "." with "," to make it excel compatible.  
	 * 
	 * @param float 
	 * @return string variant of the float
	 */
	private String fToS(float f){
		
		return String.valueOf(f).replace(".", ",");
	}
	
	private String fToS(double d){
		
		return String.valueOf(d).replace(".", ",");
	}
	
	public void writeSubjectData(ArrayList<Subject> data) throws FileNotFoundException, UnsupportedEncodingException{
		
		System.out.println("exporting file");
		
		File f = new File("\\mrt_output.csv");
		//File f = new File("C:\\Uni\\mrt_output.csv");
		
		if(f.exists()){
			f.delete();
		}
		
		PrintWriter writer = new PrintWriter("\\mrt_output.csv");
		
		writer.println("Subject;Duration;GivenAnswers;CorrectAnswers;A1;A2;A3;A4;A5;A6;A7;A8;A9;A10;A11;A12;A13;A14;A15;A16;A17;A18;A19;A20;A21;A22;A23;A24;A25;A26;A27;A28;A29;A30;A31;A32;A33;A34;A35;A36;A37;A38;A39;A40;");
		
		for (Subject sub : data){
			
			writer.print(sub.getId() + ";" +  fToS(sub.getDuration()) + ";" + sub.getGivenAnswers() + ";" + sub.getCorrectAnswers()+";");
			
			for(int answer : sub.getAnswers()){
				writer.print(answer + ";");
			}
			
			writer.println();
		}
		
		writer.close();
		
	}
	
	
	public void writeData(ArrayList<Trial> data) throws FileNotFoundException, UnsupportedEncodingException{
	
		System.out.println("exporting file");
		
		File f = new File("C:\\Uni\\mrt_output.csv");
		//File f = new File("\\mrt_output.csv");
		
		if(f.exists()){
			f.delete();
		}
		

		
		PrintWriter writer = new PrintWriter("C:\\Uni\\mrt_output.csv", "UTF-8");
		//PrintWriter writer = new PrintWriter("\\mrt_output.csv", "UTF-8");
		
		//writer.println("Subject;TaskID;TaskNumber;TrialType;Answer;TimeOnTask;SuccessRate;InitialAD;FinalAD;AverageVelocity;AccumulatedWay;NumberOfDrags;StartPhysicalPhase;EndPhysicalPhase;PhysicalPhaseDuration");
		
		
		writer.println("Subject;Group;TaskID;TaskNumber;TrialType;Answer;TimeOnTask;SuccessRate;InitialAD;FinalAD;AverageVelocity;AccumulatedWay;NumberOfDrags;StartPhysicalPhase;EndPhysicalPhase;PhysicalPhaseDuration");
		
		for (Trial t : data){
			
			writer.println(t.getSubject() + ";"+ t.getGroupID() +";"+ t.getTaskID() + ";" + t.getId()+ ";" + t.isSameTrial() + ";" + t.isCorrectAnswer() + ";"+fToS(t.getDuration()) +";" + fToS(t.getSuccessRate())  +";" + fToS(t.getInitialAD()) + ";" + fToS(t.getFinalAD()) + ";" 
						+ fToS(t.getAverageVelocity()) + ";" + fToS(t.getAccumulatedWay()) + ";" + t.getDrags().size() + ";" + fToS(t.getPhysicalPhaseStartRelative()) + ";" + fToS(t.getPhysicalPhaseEndRelative()) + ";" + fToS(t.getPhyisicalPhaseLenght()));
									
		}	
		
		writer.close();
	}
	
}
