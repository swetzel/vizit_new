package IO;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import view.GUI;
import data.FeatureVector;
import data.Trial;


public class Parser {
	
	private static Parser instance = new Parser();

	public static Parser getInstance() {
        return instance;
    }
	
	private Parser(){}
		
	
	public ArrayList<Trial> readFolder(String path, String groupID, ArrayList<Trial> trials) throws IOException{
		
		
		File file = new File(path);
		
		
		if (!file.isDirectory() &&file.getAbsolutePath().endsWith(".csv")){
					
			trials.addAll(readTrials(file.getAbsolutePath()));
		}
			
		return trials;
	}
	
	public ArrayList<Trial> readTrials(String path) throws IOException{
		
		File fileSub = new File(path);
		ArrayList<Trial> trials = new ArrayList<Trial>();
		System.out.println("Read subject " + path);
		
		FileInputStream fstream = new FileInputStream(fileSub);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;

		Trial trial = null;
		int trialNum = 0;
		
		float lastAD = -1;
		float lastX = -3;
		float lastY = -3;
		
		boolean physicalPhaseStart = false;
		
		// read trials
		while ((strLine = br.readLine()) != null){
			
			String [] split = strLine.split(";");
			
			if(!split[5].equals("NULL")){
			
				if (!split[0].equals("timestamp")){
					
					int num = Integer.parseInt(split[1]);
					
					if (num != trialNum || trialNum == 0){
						
						if (trial != null){
							trials.add(trial);
						}
						
						boolean same; 
						if (split[3].contains("1"))
							same = true;
						else 
							same = false;
							
//						boolean correct; 
//						if (split[4].contains("1"))
//							correct = true;
//						else 
//							correct = false;
						
						
						
						trial = new Trial(split[0],split[2], num, same, Integer.parseInt(split[3]), Integer.parseInt(split[4]), 1, Integer.parseInt(split[5]),Integer.parseInt(split[6]));
						trialNum = num;
						lastAD = -1;
						lastX = -3;
						lastY = -3;
						physicalPhaseStart = false;
					}
	
					float vX; 
					float vY;
					
					if (split[14].equals("NULL")){
						vX = 0;
						vY = 0;
					}
					else{
						
						vX = Float.parseFloat(split[14]); 
						vY = Float.parseFloat(split[15]); 
					if (vX == -77)
						vX = 0;
					if (vY == -77)
						vY = 0;
					}
					
					if (lastX == -3)
						lastX = vX;
					
					if (lastY == -3)
						lastY = vY;
									
					FeatureVector fv;
					
					if (split[7].equals("NULL") && split[10].equals("NULL")){
					
						fv = new FeatureVector(Float.parseFloat(split[5]), 0,0,0,0,0,0,0,0,
														 0, 0, Math.abs(vX-lastX), Math.abs(vY-lastY));
					}
					else if (!split[7].equals("NULL") && split[10].equals("NULL")){
						
						fv = new FeatureVector(Float.parseFloat(split[5]), 0,Float.parseFloat(split[7]),0,0,0,0,0,0,
								 0, 0, Math.abs(vX-lastX), Math.abs(vY-lastY));
					}
					
					else {
						
						if (lastAD == -1)
							lastAD = Float.parseFloat(split[7]);
						
						float velo = Math.abs(Float.parseFloat(split[7])- lastAD);
						if (velo > 50)
							velo = 50;
						
						if (velo > 0 && !physicalPhaseStart){
							
							physicalPhaseStart = true;
							trial.setPhysicalPhaseStart(Float.parseFloat(split[5]));
						}
							
						
						fv = new FeatureVector(Float.parseFloat(split[5]), velo, Float.parseFloat(split[7]),Float.parseFloat(split[8]),Float.parseFloat(split[9]),Float.parseFloat(split[10]),
								Float.parseFloat(split[11]),Float.parseFloat(split[12]),Float.parseFloat(split[13]), 0, 0, Math.abs(vX-lastX), Math.abs(vY-lastY));
						
						lastAD = Float.parseFloat(split[7]);
					}
					
					lastX = vX;
					lastY = vY;
					trial.addData(fv);				
			}
			}
		}
		trials.add(trial);

		
		br.close();
		return trials;
	}
}

